# vhdl files
FILES=src/hdl/*
VHDLEX=.vhd

# testbench information
TESTBENCH=top
TESTBENCH_FILE=${TESTBENCH}_tb
TESTBENCH_PATH=src/hdl/${TESTBENCH_FILE}${VHDLEX}

# ghdl configuration
GHDL=ghdl
GHDLFLAGS=--std=08

SIMDIR=simulation
STOP_TIME=31ms
GHDL_SIM_OPT=--stop-time=$(STOP_TIME)

WAVEFORM_VIEWER=gtkwave

all: compile

compile: clean
	@mkdir -p $(SIMDIR)
	$(GHDL) -i $(GHDLFLAGS) --workdir=$(SIMDIR) --work=work $(TESTBENCH_PATH) $(FILES)
	$(GHDL) -m $(GHDLFLAGS) --workdir=$(SIMDIR) --work=work $(TESTBENCH_FILE)

run: compile
	$(GHDL) -r $(GHDLFLAGS) --workdir=$(SIMDIR) --work=work $(TESTBENCH_FILE) $(GHDL_SIM_OPT) --vcd=$(SIMDIR)/$(TESTBENCH_FILE).vcd

view:
	$(WAVEFORM_VIEWER) $(SIMDIR)/$(TESTBENCH_FILE).vcd

clean:
	-rm -rf $(SIMDIR)
