library IEEE;
use IEEE.std_logic_1164.all;

entity top is
port (
    clk_i : in STD_LOGIC;
    rst_i : in STD_LOGIC;
    RXD_i : in STD_LOGIC;
    led7_an_o : out STD_LOGIC_VECTOR (3 downto 0);
    led7_seg_o : out STD_LOGIC_VECTOR (7 downto 0)
);
end top;

architecture data_flow of top is
    component rs232_monitor
        port (
            clk_i : in STD_LOGIC;
            rst_i : in STD_LOGIC;
            RXD_i : in STD_LOGIC;
            data_o : out STD_LOGIC_VECTOR (7 downto 0)
        );
    end component;

    component display
        port (
            clk_i : in STD_LOGIC;
            rst_i : in STD_LOGIC;
            digit_i : in STD_LOGIC_VECTOR (31 downto 0);
            led7_an_o : out STD_LOGIC_VECTOR (3 downto 0);
            led7_seg_o : out STD_LOGIC_VECTOR (7 downto 0)
        );
    end component;

    function byteToLedHex(value : STD_LOGIC_VECTOR (3 downto 0)) return STD_LOGIC_VECTOR is
    begin
        case value is
            when "0000" => return "0000001";
            when "0001" => return "1001111";
            when "0010" => return "0010010";
            when "0011" => return "0000110";
            when "0100" => return "1001100";
            when "0101" => return "0100100";
            when "0110" => return "0100000";
            when "0111" => return "0001111";
            when "1000" => return "0000000";
            when "1001" => return "0000100";
            when "1010" => return "0001000";
            when "1011" => return "1100000";
            when "1100" => return "0110001";
            when "1101" => return "1000010";
            when "1110" => return "0110000";
            when "1111" => return "0111000";
            when others => return "1111111";
        end case;
    end function;

    signal data_o : STD_LOGIC_VECTOR (7 downto 0);
    signal digit_i : STD_LOGIC_VECTOR (31 downto 0) := (others => '1');
begin
    rs232_monitor_0: rs232_monitor port map (
        clk_i => clk_i,
        rst_i => rst_i,
        RXD_i => RXD_i,
        data_o => data_o
    );
    display_0: display port map (
        clk_i => clk_i,
        rst_i => rst_i,
        digit_i => digit_i,
        led7_an_o => led7_an_o,
        led7_seg_o => led7_seg_o
    );

    digit_i <= (
        15 downto 9 => byteToLedHex(data_o(7 downto 4)),
        7 downto 1 => byteToLedHex(data_o(3 downto 0)),
        others => '1'
    );
end architecture data_flow;
