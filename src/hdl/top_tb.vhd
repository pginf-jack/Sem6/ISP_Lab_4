library IEEE;
use IEEE.std_logic_1164.all;

--  A testbench has no ports.
entity top_tb is
end top_tb;

architecture behav of top_tb is
  --  Declaration of the component that will be instantiated.
  component top
    port (
      clk_i : in STD_LOGIC;
      rst_i : in STD_LOGIC;
      RXD_i : in STD_LOGIC;
      led7_an_o : out STD_LOGIC_VECTOR (3 downto 0);
      led7_seg_o : out STD_LOGIC_VECTOR (7 downto 0)
    );
  end component;

  --  Specifies which entity is bound with the component.
  for top_0: top use entity work.top;
  signal clk_i : STD_LOGIC := '1';
  signal rst_i : STD_LOGIC := '1';
  signal RXD_i : STD_LOGIC := '1';
  signal led7_an_o : STD_LOGIC_VECTOR (3 downto 0);
  signal led7_seg_o : STD_LOGIC_VECTOR (7 downto 0);

  constant PERIOD : time := 10 ns;
  constant NOMINAL_BAUD_RATE : time := 1 sec / 9600;
begin
  --  Component instantiation.
  top_0: top port map (
    clk_i => clk_i,
    rst_i => rst_i,
    RXD_i => RXD_i,
    led7_an_o => led7_an_o,
    led7_seg_o => led7_seg_o
  );

  clk_i <= not clk_i after PERIOD/2;
  rst_i <= '0' after PERIOD;

  process
    type pattern_type is record
      baud_rate : time;
      value : STD_LOGIC_VECTOR (7 downto 0);
    end record;
    --  The patterns to apply.
    type pattern_array is array (natural range <>) of pattern_type;
    -- `X` means no digit, i.e. blank display (excluding dots)
    constant patterns: pattern_array := (
      (NOMINAL_BAUD_RATE, "01010011"),
      (NOMINAL_BAUD_RATE, "00000000"),
      (0.96*NOMINAL_BAUD_RATE, "01010011"),
      (0.96*NOMINAL_BAUD_RATE, "00000000"),
      (1.04*NOMINAL_BAUD_RATE, "01010011"),
      (1.04*NOMINAL_BAUD_RATE, "00000000")
    );
    variable baud_rate : time;
    variable value : STD_LOGIC_VECTOR (7 downto 0);
  begin
    wait for NOMINAL_BAUD_RATE;  -- allow RXD_i to be '1' for some time

    for i in patterns'range loop
      baud_rate := patterns(i).baud_rate;
      value := patterns(i).value;
      -- send start bit
      RXD_i <= '0';
      wait for baud_rate;
      -- transmission started, send bits
      for bit_index in 0 to value'length-1 loop
        RXD_i <= value(bit_index);
        wait for baud_rate;
      end loop;
      -- send stop bit
      RXD_i <= '1';
      wait for baud_rate;
      -- transmission stopped
      wait for 4 ms;  -- wait for whole display to refresh
    end loop;
    wait;
  end process;

  process
    type pattern_type is record
      baud_rate : time;
      value : STD_LOGIC_VECTOR (31 downto 0);
    end record;
    --  The patterns to apply.
    type pattern_array is array (natural range <>) of pattern_type;
    constant patterns: pattern_array := (
      (NOMINAL_BAUD_RATE, (15 downto 1 => "010010010000110", others => '1')),
      (NOMINAL_BAUD_RATE, (15 downto 1 => "000000110000001", others => '1')),
      (0.96*NOMINAL_BAUD_RATE, (15 downto 1 => "010010010000110", others => '1')),
      (0.96*NOMINAL_BAUD_RATE, (15 downto 1 => "000000110000001", others => '1')),
      (1.04*NOMINAL_BAUD_RATE, (15 downto 1 => "010010010000110", others => '1')),
      (1.04*NOMINAL_BAUD_RATE, (15 downto 1 => "000000110000001", others => '1'))
    );
    variable baud_rate : time;
    variable value : STD_LOGIC_VECTOR (31 downto 0);
  begin
    wait for NOMINAL_BAUD_RATE;  -- allow RXD_i to be '1' for some time

    for i in patterns'range loop
      baud_rate := patterns(i).baud_rate;
      value := patterns(i).value;
      wait for 10*baud_rate;  -- 1 start bit, 8 data bits, 1 stop bit
      for j in 0 to 3 loop
        case led7_an_o is
          when "0111" => assert led7_seg_o = value(31 downto 24);
          when "1011" => assert led7_seg_o = value(23 downto 16);
          when "1101" => assert led7_seg_o = value(15 downto 8);
          when "1110" => assert led7_seg_o = value(7 downto 0);
          when others => null;
        end case;
        wait for 1 ms;
      end loop;
    end loop;
    assert false report "end of test" severity note;
    wait;
  end process;

end behav;
